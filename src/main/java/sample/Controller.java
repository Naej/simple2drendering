package sample;

import camera.MovableJfxCamera;
import displayable.Displayable;
import displayable.JfxCircle;
import displayable.JfxPathLine;
import displayable.JfxSprite;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import java.util.ArrayList;

public class Controller {
    @FXML
    private Canvas myCanvas;
    @FXML
    private Label mousePos;

    private JfxSprite testSprite;

    private MovableJfxCamera camera;
    private ArrayList<Displayable> todisplay = new ArrayList<Displayable>();
    private JfxPathLine path = new JfxPathLine();
    private JfxCircle mouseCircle = new JfxCircle(new Point2D(0,0),5);
    @FXML
    private void initialize()
    {

        testSprite = new JfxSprite(new Point2D(0,0),new Image("/testSprite.png"));
        camera = new MovableJfxCamera(myCanvas);
        todisplay.add(path);
        todisplay.add(mouseCircle);
        todisplay.add(testSprite);
        camera.display(todisplay);
    }

    public void onMouseClick(MouseEvent e){
        if(e.getButton() == MouseButton.PRIMARY) {
            path.addNode(new JfxCircle(getMouseWorldPosition(e,camera), 10));
        } else {
            camera.setZoom(0.5);
            camera.centerOn(getMouseWorldPosition(e,camera));
        }
        onMouseMove(e);
    }
    public void onMouseMove(MouseEvent e){
        mousePos.setText("Mouse: "+ getMouseWorldPosition(e,camera) + "\n" +
                "Camera: " + camera.getViewport() + " zoom " + camera.getZoom());
        mouseCircle.setPosition(getMouseWorldPosition(e,camera));
        camera.display(todisplay);
    }

    private Point2D getMouseWorldPosition(MouseEvent e, MovableJfxCamera camera) {
        return camera.fromScreenToWorldSpace(new Point2D(e.getX(), e.getY()));
    }


}
