package displayable;

import camera.MovableJfxCamera;
import javafx.scene.image.Image;

import javafx.geometry.Point2D;

public class JfxSprite extends AbstractJfxMovableDisplayable<MovableJfxCamera> {
    private Image picture;

    public JfxSprite(Point2D position, Image picture){
        setPosition(position);
        setPicture(picture);
    }
    public void display(MovableJfxCamera camera) {
        Point2D displayedPos = camera.fromWorldToCameraSpace(getPosition());
        camera.getGraphicsContext().drawImage(picture,displayedPos.getX(),displayedPos.getY());
    }

    public void setPicture(Image picture) {
        this.picture = picture;
    }
}
