package displayable;

import camera.MovableJfxCamera;
import javafx.geometry.Point2D;

import java.util.ArrayList;

public class JfxPathLine implements Displayable<MovableJfxCamera> {
    private ArrayList<JfxCircle> nodes = new ArrayList<JfxCircle>();

    public void display(MovableJfxCamera camera) {
        JfxCircle prev = null;
        for (JfxCircle node : nodes) {
            node.display(camera);
            if(prev != null) {
                drawLine(node.position,prev.position,camera);
            }
            prev = node;
        }
    }
    private void drawLine(Point2D a,Point2D b, MovableJfxCamera camera) {
        Point2D ac = camera.fromWorldToCameraSpace(a);
        Point2D bc = camera.fromWorldToCameraSpace(b);
        camera.getGraphicsContext().strokeLine(ac.getX(),ac.getY(),bc.getX(),bc.getY());
    }

    public void addNode(JfxCircle node){
        nodes.add(node);
    }
}
