package displayable;

import camera.ICamera;

import javafx.geometry.Point2D;

public abstract class AbstractJfxMovableDisplayable<Camera extends ICamera> implements MovableDisplayable<Camera,Point2D> {
    protected Point2D position = new Point2D(0,0);


    public void setPosition(Point2D position) {
        this.position = position;
    }

    public Point2D getPosition() {
        return this.position;
    }

    public void move(Point2D vector) {
        setPosition(getPosition().add(vector));
    }
}
