package displayable;

import camera.ICamera;

public interface MovableDisplayable<Camera extends ICamera, Vector> extends Displayable<Camera> {

    /**
     * Sets the position of the drawable to the new position
     * @param position the new postion of the drawable
     */
    void setPosition(Vector position);

    /**
     * Returns the current position of the drawable
     * @return
     */
    Vector getPosition();

    /**
     * Offsets the current position of the drawable by the vector
     * @param vector the difference of position
     */
    void move(Vector vector);

}
