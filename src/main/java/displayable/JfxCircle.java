package displayable;

import camera.MovableJfxCamera;
import javafx.geometry.Point2D;

public class JfxCircle extends AbstractJfxMovableDisplayable<MovableJfxCamera> {
    float ray;

    public JfxCircle(Point2D position,float ray) {
        setPosition(position);
        this.ray = ray;
    }

    public void display(MovableJfxCamera camera) {
        Point2D displayedPos = camera.fromWorldToCameraSpace(getPosition().subtract(new Point2D(ray,ray)));
        camera.getGraphicsContext().strokeOval(displayedPos.getX(),displayedPos.getY(),ray*2,ray*2);
    }
}
