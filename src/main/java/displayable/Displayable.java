package displayable;

import camera.ICamera;

public interface Displayable<Camera extends ICamera> {
    /**
     * Allows you to display the element
     * @param camera the camera used to render the object
     */
    void display(Camera camera);
}
