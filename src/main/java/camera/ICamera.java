package camera;

import displayable.Displayable;

/**
 * Interface describing the essential functions to make a camera
 *
 */
public interface ICamera<Vector, Rectangle> {

    /**
     * Allows you to get the position of the camera
     * @return the position
     */
    Vector getPosition();

    /**
     * Allows you to know the level of zoom of the camera
     * @return the zoom of the camera
     */
    double getZoom();

    /**
     * Allows you to know the area rendered by the camera
     * @return the viewport of the camera
     */
    Rectangle getViewport();

    /**
     * Allows you to send objects that will be displayed
     * @param displayable objects that will be displayed
     */
    void display(Iterable<Displayable> displayable);

    /**
     * Allows you to clear the display of the camera
     */
    void clear();

    /**
     * Allows you to know the position where a point from the world space will be displayed in the camera space
     * @param point the point in the world space
     * @return the point in the camera space
     */
    Vector fromWorldToCameraSpace(Vector point);

    /**
     * Allows you to know the position where a point displayed in the camera space is in the world space
     * @param point the point in the camera space
     * @return the point in the world space
     */
    Vector fromCameraToWorldSpace(Vector point);

}
