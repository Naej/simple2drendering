package camera;

import displayable.Displayable;

public abstract class AbstractCamera<Vector, Rectangle> implements ICamera<Vector, Rectangle>{

    public void display(Iterable<Displayable> displayable){
        clear();
        for (Displayable d : displayable){
            d.display(this);
        }
    }
}
