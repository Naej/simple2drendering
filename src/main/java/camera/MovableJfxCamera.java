package camera;

import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.Canvas;



public class MovableJfxCamera extends AbstactJfxCameraWithScreenOffset<Point2D, Rectangle2D> implements MovableCamera<Point2D, Rectangle2D>{

    private Point2D position = new Point2D(0,0);
    private double zoom = 1;

    public MovableJfxCamera(Canvas canvas){
        super(canvas);
    }

    public void clear() {
        canvas.getGraphicsContext2D().clearRect(0,0,canvas.getHeight()*1/zoom,canvas.getWidth()*1/zoom);
        canvas.getGraphicsContext2D().strokeRect(0,0,canvas.getHeight()*1/zoom,canvas.getWidth()*1/zoom);
    }

    public Point2D getPosition() {
        return position;
    }

    public void centerOn(Point2D position) {
        setPosition(position.subtract(new Point2D(getViewport().getWidth()/2.,getViewport().getHeight()/2.)));
    }
    public double getZoom() {
        return zoom;
    }


    public Rectangle2D getViewport() {
        return new Rectangle2D(position.getX(),position.getY(),canvas.getHeight()*1/zoom,canvas.getWidth()*1/zoom);
    }

    public Point2D fromWorldToCameraSpace(Point2D point) {

        return point.subtract(getPosition());
    }

    public Point2D fromCameraToWorldSpace(Point2D point) {
        return point.add(getPosition());
    }

    public Point2D fromCameraToScreenSpace(Point2D point) {
        return point.multiply(zoom);
    }

    public Point2D fromScreenToCameraSpace(Point2D point) {
        return point.multiply(1/zoom);
    }

    public void setPosition(Point2D position) {
        this.position = position;
    }

    public void setZoom(double zoom) {
        if(zoom != 0) {
            zoom(1 / this.zoom);
            zoom(zoom);
        }
        //TODO : see how to handle the 0 value
    }

    /**
     * multiplies the zoom by the multiplier
     * @param multiplier the value that affects the zoom level
     */
    public void zoom(double multiplier) {
        canvas.getGraphicsContext2D().scale(multiplier,multiplier);
        zoom *= multiplier;
    }
}
