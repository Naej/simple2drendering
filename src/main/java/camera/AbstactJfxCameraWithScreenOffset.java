package camera;

import displayable.Displayable;
import javafx.scene.canvas.Canvas;

public abstract class AbstactJfxCameraWithScreenOffset<Vector,Rectangle> extends AbstractJavaFXCamera<Vector,Rectangle> implements CameraWithScreenOffset<Vector,Rectangle> {
    public AbstactJfxCameraWithScreenOffset(Canvas canvas) {
        super(canvas);
    }

    public Vector fromScreenToWorldSpace(Vector point) {
        return fromCameraToWorldSpace(fromScreenToCameraSpace(point));
    }

    public Vector fromWorldToScreenSpace(Vector point) {
        return fromCameraToScreenSpace(fromWorldToCameraSpace(point));
    }
}
