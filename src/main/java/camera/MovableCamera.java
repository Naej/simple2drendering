package camera;


public interface MovableCamera<Vector,Rectangle> extends ICamera<Vector,Rectangle>{
    /**
     * Allows you to set the position of the camera
     * @param position the new position of the camera
     */
    void setPosition(Vector position);

    /**
     * Allows you to set the zoom of the camera
     * @param zoom the new zoom of the camera
     */
    void setZoom(double zoom);
}
