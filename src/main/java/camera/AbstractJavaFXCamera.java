package camera;

import displayable.Displayable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

public abstract class AbstractJavaFXCamera<Vector,Rectangle> extends AbstractCamera<Vector,Rectangle> implements IJavafxCamera<Vector,Rectangle> {
    protected Canvas canvas;

    public AbstractJavaFXCamera(Canvas canvas) {
        this.canvas = canvas;
    }

    public GraphicsContext getGraphicsContext() {
        return canvas.getGraphicsContext2D();
    }
}
