package camera;


import displayable.Displayable;

/**
 * If your Camera space doesnt match your screen space you'll need those functions
 * @param <Vector>
 */
public interface CameraWithScreenOffset<Vector,Rectangle> extends ICamera<Vector,Rectangle> {
    /**
     * This function transforms a point on screen to a point in the world space
     * @param point on screen
     * @return the point in the world space
     */
    public Vector fromScreenToWorldSpace(Vector point);

    /**
     * This function transforms a point in the world space to a point on screen
     * @param point in the world space
     * @return the point on screen
     */
    public Vector fromWorldToScreenSpace(Vector point);

    /**
     * This function transforms a point in the camera space to a point on screen
     * @param point in the camera space
     * @return the point on screen
     */
    public Vector fromCameraToScreenSpace(Vector point);

    /**
     * This function transforms a point on screen to a point in the camera space
     * @param point on screen
     * @return the point in the camera space
     */
    public Vector fromScreenToCameraSpace(Vector point);

}
