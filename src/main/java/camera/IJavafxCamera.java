package camera;

import displayable.Displayable;
import javafx.scene.canvas.GraphicsContext;

public interface IJavafxCamera<Vector,Rectangle> extends ICamera<Vector,Rectangle>{
    GraphicsContext getGraphicsContext();
}
